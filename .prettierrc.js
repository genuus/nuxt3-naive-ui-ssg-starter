module.exports = {
  printWidth: 100,
  trailingComma: 'none',
  vueIndentScriptAndStyle: true,
};
