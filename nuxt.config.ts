import { defineNuxtConfig } from 'nuxt';
// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
  srcDir: './',
  build: {
    transpile: ['vueuc'], // fix dev error: Cannot find module 'vueuc'
    postcss: {
      postcssOptions: {
        plugins: {
          autoprefixer: {}
        }
      }
    }
  },
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@import "@/assets/index.scss";'
        }
      }
    },
    // @ts-expect-error: Missing ssr key
    ssr: {
      noExternal: ['moment', 'naive-ui', '@juggle/resize-observer', '@css-render/vue3-ssr']
    }
  }
});
